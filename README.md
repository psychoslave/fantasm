# Fantasm’

Assembly made cross-customery

This project aims to gather resources and solutions to make assemblies languages
usable using a more various set of linguistic sources, letting users select
or define their own prefered words for each token, and switch complete code base
between these sets of lexicological representation while preserving semantic.